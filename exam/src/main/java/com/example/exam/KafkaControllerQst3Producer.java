package com.example.exam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

//Question 3 : je vais creer un producer et un consumer(saved dans un fichier csv)
//fonction pour créer un producer
@RestController
public class KafkaControllerQst3Producer {

    private String topic = "mytopic";

    @Autowired
    private KafkaTemplate<String,String> kafkaTemplate;

    @GetMapping("question3/{message}")
    public String publishMessage(@PathVariable String message){
        kafkaTemplate.send(topic,message);
        return "Message de ma la 3eme Question envoyé :)";
    }
}
