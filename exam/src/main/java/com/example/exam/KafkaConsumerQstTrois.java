package com.example.exam;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.io.FileWriter;
import java.io.IOException;
import java.util.function.Consumer;

@Service
public class KafkaConsumerQstTrois {
    @KafkaListener(topics = "mytopic",groupId = "groupe")
    public void consume(ConsumerRecord<String, String> record) {
        try {
            String message = record.value();
            writeToCsv(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void writeToCsv(String message) throws IOException, IOException {
        // Open the CSV file for appending
        FileWriter csvWriter = new FileWriter("output.csv", true);

        // Write the message to the CSV file
        csvWriter.write(message);
        csvWriter.write("\n");

        // Close the CSV file
        csvWriter.close();
    }
}
