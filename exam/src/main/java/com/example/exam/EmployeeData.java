package com.example.exam;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;

import java.util.Date;

@Entity
public class EmployeeData {
    @Id
    private Integer businessEntityID;
    private Integer departmentID;
    private Integer shiftID;
    private Date startDate;
    private Date endDate;
    private Date modifiedDate;

    public Integer getBusinessEntityID() {
        return businessEntityID;
    }

    public void setBusinessEntityID(Integer businessEntityID) {
        this.businessEntityID = businessEntityID;
    }

    public Integer getDepartmentID() {
        return departmentID;
    }

    public void setDepartmentID(Integer departmentID) {
        this.departmentID = departmentID;
    }

    public Integer getShiftID() {
        return shiftID;
    }

    public void setShiftID(Integer shiftID) {
        this.shiftID = shiftID;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Override
    public String toString() {
        return "EmployeeShift{" +
                "businessEntityID=" + businessEntityID +
                ", departmentID=" + departmentID +
                ", shiftID=" + shiftID +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", modifiedDate=" + modifiedDate +
                '}';
    }
}

