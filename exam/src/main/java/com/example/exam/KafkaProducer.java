package com.example.exam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

@Component
public class KafkaProducer {

    @Autowired
    private KafkaTemplate<String,String> kafkaTemplate;

    @Autowired
    private DataSource dataSource;

    public void fetchAndPublishData(){
        try{
            Connection connection = dataSource.getConnection();
            Statement statement =  connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT [DepartmentID]\n" +
                    "      ,[Name]\n" +
                    "      ,[GroupName]\n" +
                    "      ,[ModifiedDate]\n" +
                    "  FROM [AdventureWorks2012].[HumanResources].[Department]\n");
            FileWriter csvWriter = new FileWriter("question2.csv");
            while (resultSet.next()){
                // Process and publish data to Kafka
                int departmentId = resultSet.getInt("DepartmentID");
                String name = resultSet.getString("Name");
                String groupName = resultSet.getString("GroupName");
                // Assuming ModifiedDate is of type java.sql.Timestamp
                String modifiedDate = resultSet.getTimestamp("ModifiedDate").toString();

                // Construct a CSV line
                String csvLine = departmentId + "," + name + "," + groupName + "," + modifiedDate;
                kafkaTemplate.send("question2topic", csvLine);

                // Write data to CSV
                csvWriter.write(csvLine);
                csvWriter.write("\n");
            }
            csvWriter.close();
            resultSet.close();
            statement.close();
            connection.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }




}
