package com.example.exam;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.support.JdbcTransactionManager;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.retry.support.RetryTemplateBuilder;

import javax.sql.DataSource;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Configuration
public class BatchConfiguration {

//    @Bean
//    @Primary
//    public FlatFileItemReader<BillingData> billingDataFlatFileItemReader(){
//        return new FlatFileItemReaderBuilder<BillingData>()
//                .name("billingDataFileReader")
//                .resource(new FileSystemResource("staging/billing-2023-01.csv"))
//                .delimited()
//                .names("dataYear","dataMonth","accountId","phoneNumber","dataUsage","callDuration","smsCount")
//                .targetType(BillingData.class)
//                .build();
//    }
    @Bean
    public Job job(JobRepository jobRepository, Step step1, Step step2){
        return new JobBuilder("myJob", jobRepository)
                .start(step1)
                .next(step2)
                .build();
    }
    @Bean
    @StepScope
    public Tasklet preparingStaging(){
        return new Tasklet() {
            @Override
            public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
                JobParameters jobParameters = contribution.getStepExecution().getJobParameters();
                //String inputFile = jobParameters.getString("input.file");
                Path source = Paths.get("src/main/resources/employee_data.csv");
                Path target = Paths.get("staging", source.toFile().getName());
                Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
                return RepeatStatus.FINISHED;
            }
        };
    }


    @Bean
    public Step step1(JobRepository jobRepository, JdbcTransactionManager transactionManager){
        return new StepBuilder("filePreparation", jobRepository)
                .tasklet(preparingStaging(), transactionManager)
                //.chunk(100,transactionManager)
                .build();
    }
    @Bean
    public RetryTemplate retryTemplate() {
        return new RetryTemplateBuilder()
                .maxAttempts(3)
                .build();


    }


    @Bean
    public Step step2(JobRepository jobRepository,
                      JdbcTransactionManager jdbcTransactionManager,
                      ItemReader<EmployeeData> DataItemReader,
                      ItemWriter<EmployeeData> DataItemWriter,
                      RetryTemplate retryTemplate){
        double chunk = Math.ceil(ligneTotal()*0.05);
        return new StepBuilder("fileIngestion",jobRepository)
                .<EmployeeData,EmployeeData>chunk((int)chunk,jdbcTransactionManager)
                .reader(DataItemReader)
                .writer(DataItemWriter)
                .faultTolerant()
                .retry(Exception.class)
                .skipLimit(7) //Le programme doit tolérer jusqu’à 7% d’erreur
                .build();

    }
    private double ligneTotal(){
        return 0;
    }
    @Bean
    public JdbcBatchItemWriter<EmployeeData> billingDataJdbcBatchItemWriter(DataSource dataSource){
        String sql = "insert into EMPLOYEE_DATA values (:businessEntityID, :departmentID, :shiftID, :startDate, :endDate, :modifiedDate)";
        return new JdbcBatchItemWriterBuilder<EmployeeData>()
                .dataSource(dataSource)
                .sql(sql)
                .beanMapped()
                .build();
    }

    @Bean
    public FlatFileItemReader<EmployeeData> DataFlatFileItemReader(){
        return new FlatFileItemReaderBuilder<EmployeeData>()
                .name("DataFileReader")
                .resource(new FileSystemResource("staging/employee_data.csv"))
                .delimited()
                .names("businessEntityID","departmentID","shiftID","startDate","endDate","modifiedDate")
                .targetType(EmployeeData.class)
                .build();
    }







}
